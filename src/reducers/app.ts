export interface iAction {
    type: string,
    payload: any
}
export type iState = Array<iPlayer>
export interface iReducer {
    (state: iState, action: iAction): iState
}
export interface iPlayer {
    name: string
    wonder: string
    total: number
    points: {
        military: number
        money: number
        debt: number
        wonder: number
        civic: number
        commerce: number
        science: {
            gears: number
            tablets: number
            harps: number
        }
        guild: number
        leaders: number
        cities: number
    }
}
export const initialState: iState = [{
    name: '',
    wonder: '',
    total: 0,
    points: {
        military: 0,
        money: 0,
        debt: 0,
        wonder: 0,
        civic: 0,
        commerce: 0,
        science: {
            gears: 0,
            tablets: 0,
            harps: 0
        },
        guild: 0,
        leaders: 0,
        cities: 0
    }
}, {
    name: '',
    wonder: '',
    total: 0,
    points: {
        military: 0,
        money: 0,
        debt: 0,
        wonder: 0,
        civic: 0,
        commerce: 0,
        science: {
            gears: 0,
            tablets: 0,
            harps: 0
        },
        guild: 0,
        leaders: 0,
        cities: 0
    }
}, {
    name: '',
    wonder: '',
    total: 0,
    points: {
        military: 0,
        money: 0,
        debt: 0,
        wonder: 0,
        civic: 0,
        commerce: 0,
        science: {
            gears: 0,
            tablets: 0,
            harps: 0
        },
        guild: 0,
        leaders: 0,
        cities: 0
    }
}]
export const init = (initialState) => {
    return initialState;
}
export const reducer: iReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_PLAYER':
            return [
                ...state, {
                    name: '',
                    wonder: '',
                    points: {
                        military: 0,
                        money: 0,
                        debt: 0,
                        wonder: 0,
                        civic: 0,
                        commerce: 0,
                        science: {
                            gears: 0,
                            tablets: 0,
                            harps: 0
                        },
                        guild: 0,
                        leaders: 0,
                        cities: 0
                    }
                }]
            break;
        case 'UPDATE_PLAYER':
            return [
                ...state.slice(0, action.payload.idx),
                action.payload.player,
                ...state.slice(action.payload.idx + 1)
            ]
            break;
        case 'SORT_BY_TOTAL':
            return state.sort((a, b) => {
                if (a.total < b.total)
                    return 1
                if (a.total > b.total)
                    return -1
            })
            break;
        case 'reset':
            return init(action.payload)
            break;

        default:
            return state;
            break;
    }

}