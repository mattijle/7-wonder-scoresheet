import React from "react"
import { PageProps, Link } from "gatsby"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons';
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import PlayerList from '../components/playerlist'
import PointView from "../components/pointview";
import VIEWS from '../constants/views';

import { reducer, initialState, init } from '../reducers/app';
import { iState, iReducer } from '../reducers/app';
interface DataProps { }

/*
<button onClick={(ev) => { setview(0) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-blue-500   ${view === 0 && 'border-t-2 border-b-2 border-gray-100'}`}>Players</button>
            <button onClick={(ev) => { setview(1) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-red-600    ${view === 1 && 'border-t-2 border-b-2 border-gray-100'}`}>Military</button>
            <button onClick={(ev) => { setview(2) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-orange-900 ${view === 2 && 'border-t-2 border-b-2 border-gray-100'}`}>Money</button>
            <button onClick={(ev) => { setview(3) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-orange-800 ${view === 3 && 'border-t-2 border-b-2 border-gray-100'}`}>Debt</button>
            <button onClick={(ev) => { setview(4) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-yellow-500 ${view === 4 && 'border-t-2 border-b-2 border-gray-100'}`}>Wonder</button>
            <button onClick={(ev) => { setview(5) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-blue-700   ${view === 5 && 'border-t-2 border-b-2 border-gray-100'}`}>Civic</button>
            <button onClick={(ev) => { setview(6) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-yellow-600 ${view === 6 && 'border-t-2 border-b-2 border-gray-100'} `}>Commerce</button>
            <button onClick={(ev) => { setview(7) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-green-700  ${view === 7 && 'border-t-2 border-b-2 border-gray-100'}`}>Science</button>
            <button onClick={(ev) => { setview(8) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-purple-700 ${view === 8 && 'border-t-2 border-b-2 border-gray-100'}`}>Guild</button>
            <button onClick={(ev) => { setview(9) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-gray-600   ${view === 9 && 'border-t-2 border-b-2 border-gray-100'}`}>Leaders</button>
            <button onClick={(ev) => { setview(10) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-gray-800  ${view === 10 && 'border-t-2 border-b-2 border-gray-100'}`}>Cities</button>
            <button onClick={(ev) => { setview(11) }} className={`w-full block py-1 px-2 mb-1 rounded-lg uppercase focus:outline-none text-white font-semibold bg-green-500 ${view === 11 && 'border-t-2 border-b-2 border-gray-100'}`}>Results</button>
*/


const IndexPage: React.FC<PageProps<DataProps>> = ({ }) => {
  const [state, dispatch] = React.useReducer<iReducer, iState>(reducer, initialState, init)
  const [view, setview] = React.useState(0)
  React.useEffect(() => { console.log('State Change', state) }, [state])
  return (
    <Layout className={`${VIEWS[view].color}`}>
      <SEO title="Home" />
      <div className={`container ${VIEWS[view].color} shadow-xl rounded-2xl pb-0`}>
        <div className={`flex flex-wrap`}>
          <div className="mb-4 py-2 w-3/12 pl-2 md:w-1/12 ">
            {VIEWS.map((v, idx) => <button onClick={(ev) => { setview(idx) }} className={`w-full block py-1 px-2 mb-1 rounded-lg text-sm uppercase focus:outline-none text-white font-semibold ${v.color}   ${view === idx && 'border-t-2 border-b-2 border-gray-100'}`}>{v.name.toUpperCase()}</button>)}

          </div>
          <div className={`min-h-full w-9/12 px-2 md:w-11/12 ${VIEWS[view].color}`}>
            <div className="min-h-full flex-wrap rounded-md">




              {view === 0 && <PlayerList players={state} dispatch={dispatch} />}
              {view !== 0 && <PointView players={state} dispatch={dispatch} view={view} />}
            </div>

          </div>
          <div className="mt-4 mb-0 h-10 py-2 w-full bg-white flex justify-between rounded-b-2xl">
            {view > 0 && <button className="px-2 mr-2 border-gray-600" onClick={() => { setview(view - 1) }}>&lt; Prev</button>}
            {view === 0 && <span className="px-2 mr-2 text-gray-400">&lt; Prev</span>}
            {view === 0 && <button title="Add Player" className="px-2 mr-2 border-gray-600" onClick={() => { dispatch({ type: 'ADD_PLAYER', payload: {} }) }}>+ <FontAwesomeIcon icon={faUser} /></button>}
            {view !== 0 && <div className="px-2 mr-2 text-gray-400">+ <FontAwesomeIcon icon={faUser} /></div>}
            {view < 11 && <button className="px-2 mr-2 border-gray-600" onClick={() => { setview(view + 1) }}>Next &gt;</button>}
            {view === 11 && <span className="px-2 mr-2 text-gray-400">Next &gt;</span>}
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage
