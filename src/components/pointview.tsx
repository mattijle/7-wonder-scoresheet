import React from 'react';
import { iPlayer } from '../reducers/app'
import VIEWS from '../constants/views'
interface PlayerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    player: iPlayer
    idx: number
    dispatch: Function
    view: number
}

const handleChange = (ev: React.ChangeEvent<HTMLInputElement>, ref: React.MutableRefObject<any>, player: iPlayer, idx: number, dispatch: Function) => {
    dispatch({
        type: 'UPDATE_PLAYER',
        payload: {
            idx,
            player: {
                ...player,
                points: {
                    ...player.points,
                    [ev.target.name]: parseInt(ref.current.value)
                }
            }
        }
    })
}
const handleScienceChange = (ev: React.ChangeEvent<HTMLInputElement>, ref: React.MutableRefObject<any>, player: iPlayer, idx: number, dispatch: Function) => {
    dispatch({
        type: 'UPDATE_PLAYER',
        payload: {
            idx,
            player: {
                ...player,
                points: {
                    ...player.points,
                    science: {
                        ...player.points.science,
                        [ev.target.name]: parseInt(ref.current.value)
                    }
                }
            }
        }
    })
}
interface ListProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    players: Array<iPlayer>
    dispatch: Function
    view: number
}
const Player: React.FC<PlayerProps> = ({ player, idx, dispatch, view, className }) => {
    const pointsEl = React.useRef(null)
    return (
        <div className={`${className} w-full mb-3 mt-3 flex`}>
            <div className={`w-8/12 py-1 px-1 rounded-l-md text-white ${idx % 2 == 0 && VIEWS[0].color} ${idx % 2 && 'bg-blue-400'}`}>
                {player.name}
            </div>
            <input ref={pointsEl} type="number" name={VIEWS[view].name} value={player.points[VIEWS[view].name]} className="w-4/12 py-1 px-1 border rounded-r-md" onChange={(ev) => { handleChange(ev, pointsEl, player, idx, dispatch) }} />

        </div >
    )
}
const PointDiv: React.FC<React.HTMLAttributes<HTMLElement>> = ({ children, className }) => {
    return (
        <div className={`py-1 px-1 h-16 opacity-50 bg-white text-black  ${className}`}><div className="mt-4">{children}</div></div>
    )
}
const ResultPlayer: React.FC<PlayerProps> = ({ player, idx, dispatch, view, className }) => {
    const [Total, setTotal] = React.useState(0)
    React.useEffect(() => {
        const total = Object.keys(player.points).reduce((prev, key) => {
            if (key !== 'science')
                return prev + player.points[key]
            if (key === 'science')
                return prev + calculateScience(player.points[key])
        }, 0)
        dispatch({
            type: 'UPDATE_PLAYER',
            payload: {
                idx: idx,
                player: {
                    ...player,
                    total
                }
            }
        })
    }, [])
    const calculateScience = (science) => {
        const sets = Math.min(science.harps, science.gears, science.tablets) * 7
        const harps = Math.pow(science.harps, 2);
        const tablets = Math.pow(science.tablets, 2);
        const gears = Math.pow(science.gears, 2);
        return sets + harps + tablets + gears

    }
    return (
        <div className={`${className} w-full mb-3 mt-3 flex`}>
            <div className={`w-1/12 py-1 px-1 h-16 rounded-l-md text-white ${idx % 2 == 0 && VIEWS[0].color} ${idx % 2 && 'bg-blue-400'}`}>{player.name} <br /> {player.wonder}</div>
            {Object.keys(player.points).map((key) => {
                if (key !== 'science') {
                    return <div key={key} className={`w-1/12 h-16 text-center text-white ${VIEWS.find(el => el.name === key).color}`}><PointDiv>{player.points[key]}</PointDiv></div>
                }
                if (key === 'science') {
                    return <div key={key} className={`w-1/12 h-16 text-center text-white ${VIEWS.find(el => el.name === key).color}`}><PointDiv>{calculateScience(player.points[key])}</PointDiv></div>
                }
            })}
            <div className={`w-1/12 py-1 px-1 h-16 rounded-r-md text-center text-white ${VIEWS[11].color}`}><div className="mt-4">{player.total}</div></div>
        </div >
    )
}
const SciencePlayer: React.FC<PlayerProps> = ({ player, idx, dispatch, view, className }) => {
    const gearsEl = React.useRef(null)
    const tabletsEl = React.useRef(null)
    const harpsEl = React.useRef(null)
    return (
        <div className={`${className} w-full mb-3 mt-3 flex`}>
            <div className={`w-8/12 py-1 px-1 rounded-l-md text-white ${idx % 2 == 0 && VIEWS[0].color} ${idx % 2 && 'bg-blue-400'}`}>
                {player.name}
            </div>
            <input ref={gearsEl} type="number" name="gears" value={player.points[VIEWS[view].name].gears} className="w-1/12 py-1 px-1 mr-1 text-center border rounded-md" onChange={(ev) => { handleScienceChange(ev, gearsEl, player, idx, dispatch) }} />
            <input ref={tabletsEl} type="number" name="tablets" value={player.points[VIEWS[view].name].tablets} className="w-1/12 py-1 px-1 mr-1 text-center border rounded-md" onChange={(ev) => { handleScienceChange(ev, tabletsEl, player, idx, dispatch) }} />
            <input ref={harpsEl} type="number" name="harps" value={player.points[VIEWS[view].name].harps} className="w-1/12 py-1 px-1 text-center border rounded-md" onChange={(ev) => { handleScienceChange(ev, harpsEl, player, idx, dispatch) }} />
            <div className="sm:w-1/12 py-1 px-1 text-center">{Math.min(player.points.science.gears, player.points.science.tablets, player.points.science.harps)}</div>
        </div >
    )
}
const ScienceView: React.FC<ListProps> = ({ players, className, dispatch, view }) => {
    return (
        <>
            <div className={`flex w-full px-2 ${VIEWS[view].color}  text-white font-semibold`}>
                <div className="w-8/12 py-1 text-center uppercase text-sm font-semibold">Player</div>
                <div className="w-1/12 py-1 text-center uppercase text-sm font-semibold">Gears</div>
                <div className="w-1/12 py-1 text-center uppercase text-sm font-semibold">Tablets</div>
                <div className="w-1/12 py-1 text-center uppercase text-sm font-semibold">Harps</div>
                <div className="w-1/12 py-1 text-center uppercase text-sm font-semibold">Sets</div>
            </div>
            <div className="min-h-full rounded-md  bg-gray-300 px-2 py-2">
                {
                    players.map((player, idx) => {
                        return <SciencePlayer key={idx} idx={idx} player={player} dispatch={dispatch} view={view} />
                    })
                }
            </div>
        </>
    )
}
const ResultsView: React.FC<ListProps> = ({ players, className, dispatch, view }) => {
    return (
        <>
            <div className={`flex w-full px-2 ${VIEWS[view].color}  text-white font-semibold`}>

                {VIEWS.map((v) => {
                    return (<>
                        <div key={v.name} className={`hidden sm:w-1/12 text-sm py-1 px-1 uppercase text-center ${v.color}`}>{v.name}</div>
                        <div key={v.name} className={`w-1/12 sm:hidden text-sm py-1 px-1 uppercase text-center ${v.color}`}>{v.name.charAt(0)}</div>
                    </>)
                })}

            </div>
            <div className="min-h-full rounded-md bg-gray-300 px-2 py-2">
                {
                    players.map((player, idx) => {
                        return <ResultPlayer key={idx} idx={idx} player={player} dispatch={dispatch} view={view} className={`${idx === 0 && 'border-t-4 border-b-4 rounded-md border-yellow-600'}`} />

                    })
                }
            </div>
        </>
    )
}

export const PointView: React.FC<ListProps> = ({ players, className, dispatch, view }) => {
    React.useEffect(() => {
        dispatch({ type: 'SORT_BY_TOTAL' })
    })
    return (
        <>
            { (view !== 11 && view !== 7) && <div className={className}>
                <div className={`flex w-full px-2 ${VIEWS[view].color}  text-white font-semibold`}>
                    <div className="w-8/12 py-1 text-center uppercase text-sm font-semibold">Player</div>
                    <div className="w-4/12 py-1 text-center uppercase text-sm font-semibold">Points</div>
                </div>
                <div className="min-h-full rounded-md bg-white px-2 py-2">
                    {players.map((player, idx) => {
                        return <Player key={idx} idx={idx} player={player} dispatch={dispatch} view={view} />
                    })}
                </div>
            </div>}
            {(view === 7) && <ScienceView players={players} dispatch={dispatch} view={view} />}
            {(view === 11) && <ResultsView players={players} dispatch={dispatch} view={view} />}
        </>
    )
}

export default PointView