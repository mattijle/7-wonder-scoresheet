import React from 'react';
import { iPlayer } from '../reducers/app'
import VIEWS from '../constants/views'
interface PlayerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    player: iPlayer
    idx: number
    dispatch: Function
    view: number
}
const handleChange = (ev: React.ChangeEvent<HTMLInputElement>, ref: React.MutableRefObject<any>, player: iPlayer, idx: number, dispatch: Function) => { }
const Player: React.FC<PlayerProps> = ({ player, idx, dispatch, view, className }) => {
    const pointsEl = React.useRef(null)
    return (
        <div className={`${className} w-full mb-3 mt-3 flex`}>
            <div className={`w-8/12 py-1 px-1 rounded-l-md text-white ${idx % 2 == 0 && VIEWS[0].color} ${idx % 2 && 'bg-blue-400'}`}>
                {player.name}
            </div>
            <input ref={pointsEl} type="number" name={VIEWS[view].name} value={player.points[VIEWS[view].name]} className="w-4/12 py-1 px-1 border rounded-r-md" onChange={(ev) => { handleChange(ev, pointsEl, player, idx, dispatch) }} />

        </div >
    )
}