import React from 'react';
import { iPlayer } from '../reducers/app'
import VIEWS from '../constants/views';
interface ListProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    players: Array<iPlayer>
    dispatch: Function
}
interface PlayerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    player: iPlayer
    players: Array<iPlayer>
    idx: number
    dispatch: Function
    availableWonders: Array<string>
}

const WONDERS = ['Giza', 'Olympia', 'Rhodes', 'Babylon', 'Halicarnassus', 'Alexandria', 'Ephesus', 'Great Wall', 'Abu Simbel', 'Stonehenge', 'Mannekin Pis', 'Petra', 'Byzantium', 'Rome', 'Siracusa', 'Catan']

const Player: React.FC<PlayerProps> = ({ player, players, idx, dispatch, className, availableWonders }) => {
    const nameEl = React.useRef(null)
    const selectEl = React.useRef(null)
    const [error, setError] = React.useState<boolean>(false)
    const handleChange = (ev, ref) => {
        dispatch({
            type: 'UPDATE_PLAYER',
            payload: {
                idx,
                player: {
                    ...player,
                    [ev.target.name]: ref.current.value
                }
            }
        })
    }
    React.useEffect(() => {
        const count = players.reduce((val, plr) => {
            if (plr.name !== player.name) {
                if (plr.wonder === player.wonder) {
                    return val + 1;
                }
            }
            return val;
        }, 0)
        if (count > 0) {
            setError(true);
        } else {
            setError(false);
        }
    }, [players])
    return (
        <div className={`${className} w-full mb-3 mt-3 flex`}>
            <div className={`w-8/12 py-1 px-1 rounded-l-md text-white ${idx % 2 == 0 && VIEWS[0].color} ${idx % 2 && 'bg-blue-400'}`} > {player.name && <div className="">{player.name}</div>}
                {!player.name && <div className="flex"><input ref={nameEl} type="text" className="w-10/12 py-1 px-1 text-black border border-red-600 rounded-md bg-gray-200" placeholder="Name..." /><button name="name" onClick={(ev) => { handleChange(ev, nameEl) }} className="relative px-2 my-1 rounded bg-blue-300" style={{ left: '-3.25rem' }}>Save</button></div>}
            </div>
            <select ref={selectEl} defaultValue={player.wonder} placeholder="Wonder" name="wonder" className={"w-4/12 py-1 px-1 border rounded-r-md " + `${error && 'border-red-600 bg-red-500 text-white'}`} onChange={(ev) => { handleChange(ev, selectEl) }}>
                <option value='' className="py-1 px-1 border rounded-md"></option>
                {availableWonders.map(wnd => {
                    return <option key={wnd} value={wnd}>{wnd}</option>
                })}
            </select>
        </div >
    )
}

export const PlayerList: React.FC<ListProps> = ({ players, className, dispatch }) => {

    return (
        <div className={className}>
            <div className="flex w-full px-2 bg-blue-500 text-white ">
                <div className="w-8/12 py-1 text-center uppercase text-sm font-semibold">Player</div>
                <div className="w-4/12 py-1 text-center uppercase text-sm font-semibold">Wonder</div>
            </div>
            <div className="min-h-0 rounded-md  bg-gray-300 px-2 py-2">
                {players.map((player, idx) => {
                    return <Player key={idx} idx={idx} player={player} players={players} dispatch={dispatch} availableWonders={WONDERS} />
                })}
            </div>
        </div>
    )
}
export default PlayerList