/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
interface LayoutProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> { }

const Layout: React.FC<LayoutProps> = ({ children, className }) => {

  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div className="mx-auto w-screen">
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <div
        className={"container my-0 mx-auto pt-0 px-4 pb-6 bg-gray-900" + className}

      >
        <main>{children}</main>

      </div>
      <footer className="mt-20 w-full mx-auto text-center" >
        © {new Date().getFullYear()}, Matti Leppänen
        </footer>
    </div>
  )
}
export default Layout
