
import React from 'react';
import { Link } from 'gatsby';
interface Props extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
    menuOpen: boolean
}
export const NavBar = ({ menuOpen, className }) => {


    return (

        <nav className={`${!menuOpen ? "hidden" : "block"} px-4 pt-3 pb-6 w-full bg-purple-800 md:bg-purple-700 ${className}`}>

        </nav>

    )
}

NavBar.defaultProps = {
    menuOpen: false,
    className: ''
}
export default NavBar;