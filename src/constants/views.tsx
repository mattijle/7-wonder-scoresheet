export default [
    { name: 'players', color: 'bg-blue-500' },
    { name: 'military', color: 'bg-red-600' },
    { name: 'money', color: 'bg-orange-900' },
    { name: 'debt', color: 'bg-orange-800' },
    { name: 'wonder', color: 'bg-yellow-500' },
    { name: 'civic', color: 'bg-blue-700' },
    { name: 'commerce', color: 'bg-yellow-600' },
    { name: 'science', color: 'bg-green-700' },
    { name: 'guild', color: 'bg-purple-700' },
    { name: 'leaders', color: 'bg-gray-600' },
    { name: 'cities', color: 'bg-gray-800' },
    { name: 'results', color: 'bg-green-500' }
]